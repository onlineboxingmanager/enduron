import {BaseObject} from "@/models/BaseObject";
import Client from "@/client";
import {capitalize} from "@/helper";

import {updateStoreOutsideSvelte} from "@/store";

export abstract class BaseDAO extends BaseObject
{

	public update()
	{
		return this.store();
	}

	public save()
	{
		return this.store();
	}

	public async delete(delete_forever: boolean = false)
	{
		this.is_deleted = true;
		return await this.store({delete_forever: delete_forever});
	}

	public async store(params: any = {})
	{
		let request = this.toJson();
		let url = this.getClassname() + "/" + this.getUid();

		let method = 'PUT'; // update
		if( this._is_new )
		{
			method = 'POST'; // add
			url = this.getClassname();
		}

		if( this.is_deleted )
		{
			method = 'DELETE';
			request = params;

			updateStoreOutsideSvelte(this.getClassname(), this.getUid(), this, 'delete' )
		}

		const response = await Client.request(url, method, request);
		if( !response )
			return this;

		this.loadFromObject(response);

		if( !this.is_deleted )
			updateStoreOutsideSvelte(this.getClassname(), this.getUid(), this, 'update' )

		return this;
	}

	public static async getAll(limit: number = 2000)
	{
		let classname = capitalize(this.CLASSNAME);
		const response = await Client.request(classname + "/all", 'GET', {limit: limit});

		return this.getCollection(response);
	}

	public static async fetchAll()
	{
		return await this.getAll();
	}

	public static async find(id: any)
	{
		let classname = capitalize(this.CLASSNAME);
		const response = await Client.request(classname + "/" + id, 'GET');

		return this.getData(response);
	}

	public static async getById(id: number)
	{
		return await this.find(id);
	}

	public static async getBySearch(params: Object)
	{
		let classname = capitalize(this.CLASSNAME);
		const response = await Client.request(classname + "/filter", 'GET', params);

		return this.getCollection(response);
	}

	public static async getListByReference(reference)
	{
		// @ts-ignore
		let myModel = new this;

		// works with single-to-many
		const refkey = Object.keys(reference.getPrimaryKeys())[0]
		return await this.getBySearch({[reference.constructor.CLASSNAME]: {[refkey]: reference.getUid()}});
	}

	public static getData(obj, update_store: boolean = true ): BaseObject
	{
		// @ts-ignore
		let myModel = new this;
		myModel.loadFromObject(obj);

		if( update_store )
			updateStoreOutsideSvelte(myModel.getClassname(), myModel.getUid(), myModel )

		return myModel
	}

	public static getCollection(data): Map<number, BaseObject>
	{
		let collection = new Map<any, BaseObject>;

		let i = 0;

		for (let obj of data.collection ?? [])
		{
			// @ts-ignore
			let myModel = this.getData(obj)
			// @ts-ignore
			myModel.id = i++;
			collection.set(myModel.getUid(), myModel)
		}

		// @ts-ignore
		collection.limit = data.limit
		// @ts-ignore
		collection.offset = data.offset
		// @ts-ignore
		collection.total = data.total
		// @ts-ignore
		collection.page = data.page
		// @ts-ignore
		collection.schema = data.schema

		return collection;
	}

}