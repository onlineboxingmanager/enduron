import {capitalize} from "@/helper";

export abstract class BaseObject
{
	public static CLASSNAME: string = '';
	protected static primarykeys = {};
	protected static properties = {};
	protected static relations = {};
	protected modified: Map<string, boolean> = new Map()
	protected loaded: Map<string, boolean> = new Map()
	protected _is_new: boolean = true;
	protected is_deleted: boolean = false;

	public constructor()
	{
	}

	public get isNew(): boolean
	{
		return this._is_new;
	}

	public set isNew(value: boolean)
	{
		this._is_new = value;
	}

	public getRelations()
	{
		// @ts-ignore
		return this.constructor.relations;
	}

	public getRelation(key: string, lazyLoad: boolean = false): BaseObject
	{
		if( typeof this[key] != 'undefined' )
			return this[key];

		if( typeof this['ref_' + key] != 'undefined' )
			return this['ref_' + key];
	}

	public getModified(): Map<string, boolean>
	{
		// @ts-ignore
		return this.modified
	}

	public getProperties(): Object
	{
		// @ts-ignore
		return this.constructor.properties;
	}

	public getProperty(keyname: string): Object
	{
		// @ts-ignore
		return this.constructor.properties[keyname];
	}

	public getPrimaryKeys()
	{
		// @ts-ignore
		return this.constructor.primarykeys;
	}

	public getDefaultname(): string
	{
		// @ts-ignore
		let name = <string> this.getUid();

		// @ts-ignore
		for ( let keyname in this.constructor.properties )
		{
			// @ts-ignore
			let property = this.constructor.properties[keyname];

			if( property.typename == 'VARCHAR' || property.typename == 'CHAR' )
			{
				// @ts-ignore
				name = this[keyname];
				break
			}
		}

		// TODO: geht nicht wegen async
		if( typeof this['getTranslationPlaceholder'] !== 'undefined' )
		{
			//return this.getTranslation();
		}

		return name;
	}

	public async getTranslation(languageiso2: string = 'de')
	{
		return false;

		const placeholderid = this.getAttribute('translationplaceholderid');
		// @ts-ignore
		const myPlaceholder = this.getTranslationPlaceholder();
		console.log("placeholder", myPlaceholder);

		//return await TranslationContent.getBySearch({translationplaceholderid: placeholderid, languageiso2:
		// languageiso2})

		/*
		if( myPlaceholder && typeof myPlaceholder['getTranslationContentList'] === 'function' )
			return [await myPlaceholder.getTranslationContentList()].filter(translation => translation.languageiso2 = languageiso2)[0]

		return ''
		 */
	}

	public getAttribute(key: string): any
	{
		if( typeof this[key] == "function" )
			return this[key]();

		return this[key]
	}

	public setAttribute(key: string, value: any)
	{
		this[key] = value;
	}

	public getUid(): string
	{
		// @ts-ignore
		for ( let keyname in this.constructor.properties )
		{
			// @ts-ignore
			let property = this.constructor.properties[keyname];
			if( property.is_primarykey === true )
			{
				return this.getAttribute(keyname);
			}
		}

		return '';
	}

	public static castTo(name, obj): BaseObject
	{
		return Object.assign(this, obj)
		//return (new Media).loadFromObject(obj)
	}

	public castTo<T>(className: new () => T, obj: any,): T
	{
		// @ts-ignore
		return new className().loadFromObject(obj);
	}

	public static test(data)
	{
		console.log("statuc", data);
	}

	public loadFromObject(object: any): this
	{
		for(let key in object )
		{
			if( key.match(/^ref_/giu) && typeof object[key] == 'object' )
			{
				// @ts-ignore
				let className = this.constructor.relations[key];
				let castToName = 'castTo' + className;

				if( typeof this[castToName] !== 'undefined')
				{
					this[key] = this[castToName](object[key])
				}
			}

			else
			{
				// @ts-ignore
				this[key] = object[key];
			}
		}

		this._is_new = false;

		return this;
	}

	public toJson(): string
	{
		let json = {};
		// @ts-ignore
		for ( let keyname in this.constructor.properties )
		{
			json[keyname] = this.getAttribute(keyname)
			// TODO: timestamp in integer wieder umwandeln

			// @ts-ignore
			if( this.constructor.properties[keyname].typename == 'TIMESTAMP' )
			{
				json[keyname] = new Date(json[keyname]).getTime();
			}

		}

		return JSON.stringify(json)
	}

	protected _setIsLoaded(key: string)
	{
		this.loaded.set(key, true)
	}

	protected _getIsLoaded(key: string): boolean
	{
		return this.loaded.has(key)
	}

	protected setClassname(value: string)
	{
		// @ts-ignore
		this.constructor.CLASSNAME = value;
	}

	public getClassname()
	{
		// darf niemals verwendet werden. Bei mehreren Objekten die hier Erben wird immer das letzte Initialisierte
		// in diese Statische Property geschrieben
		//return BaseObject.CLASSNAME

		// @ts-ignore
		return (<any> capitalize(this.constructor.CLASSNAME));
	}

	protected _setModified(key: string): void
	{
		// @ts-ignore
		this.modified.set(key, true)
	}

	protected _hasModifies(key: string): boolean
	{
		// @ts-ignore
		return this.modified.has(key)
	}

	protected _setProperties(data: any)
	{
		// @ts-ignore
		this.constructor.properties = data;
	}

}