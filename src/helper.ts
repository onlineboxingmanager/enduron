export default function show(element: HTMLElement | null) {
    console.log(element)

    if (!element)
        return;

    element.classList.remove('hidden');

    let type = 'block';
    if (element.classList.contains('flex') || element.classList.contains('weUtils__dFlex') || element.classList.toString().match('flex'))
        type = 'flex';

    if (element.tagName == 'SPAN')
        type = 'inline';

    if (element.tagName == 'TR')
        type = 'table-row';

    element.style.setProperty("display", type, "important");
}

export function hide(element: HTMLElement | null) {
    if (element)
        element.style.setProperty("display", "none", "important");
}

export function showLoader(): void {
    document.querySelectorAll('.loader').forEach(e => show(<HTMLElement>e));
}

export function hideLoader(): void {
    document.querySelectorAll('.loader').forEach(e => show(<HTMLElement>e));
}

export function setCookie(cname: string, cvalue: any, exdays: number): void {
    // expire the old cookie if existed to avoid multiple cookies with the same name
    if (getCookie(cname)) {
        document.cookie = cname + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
    let d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
}

export function getCookie(cname: string): string {
    let name = cname + "=";
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }

    return '';
}

import translation_json from "./language.json"

let translations = translation_json

export function setTranslations(data)
{
    translations = data
}

export function getTranslation(key: string): string
{
    if( !key )
        return '';

    const keyArray = key.split('__');

    let value = translations;
    for (const k of keyArray) {
        value = value[k];
    }

   //if( ! translations || typeof translations[key] === 'undefined' )
   //    return key

   return value ?? key;
}

export function tr(key: string): string
{
    return getTranslation(key)
}

export function trans(key: string): string
{
    return getTranslation(key)
}

export function capitalize(str: string): string
{
    let result: string = '';
    let capitalizeNextLetter: boolean = true;
    for (let i = 0; i < str.length; i++) {
        let currentLetter: string = str.charAt(i);
        if (currentLetter === '_') {
            capitalizeNextLetter = true;
            continue;
        }
        if (capitalizeNextLetter) {
            result += currentLetter.toUpperCase();
            capitalizeNextLetter = false;
        } else {
            result += currentLetter;
        }
    }
    return result;
}

export function ucwords(input: string): string {
    return input.replace(/\b\w/g, (char) => char.toUpperCase());
}

export function objectToURLParams(obj)
{
    const params = new URLSearchParams();

    for (const key in obj) {
        if (typeof obj[key] === 'object') {
            for (const innerKey in obj[key]) {
                params.append(key + '[' + innerKey + ']', obj[key][innerKey]);
            }
        } else {
            params.append(key, obj[key]);
        }
    }

    return params.toString();
}

export function site_url(url)
{
    return '/' + url
}

export const compress = async (
    str: string,
    encoding = 'gzip' as CompressionFormat
): Promise<ArrayBuffer> => {
    const byteArray = new TextEncoder().encode(str)
    const cs = new CompressionStream(encoding)
    const writer = cs.writable.getWriter()
    writer.write(byteArray)
    writer.close()
    return new Response(cs.readable).arrayBuffer()
}


export const decompress = async (url) => {
    const ds = new DecompressionStream('gzip');
    const response = await fetch(url);
    const blob_in = await response.blob();
    const stream_in = blob_in.stream().pipeThrough(ds);
    const blob_out = await new Response(stream_in).blob();
    return await blob_out.text();
};

export const line = (key) =>
{
    return getTranslation(key)
}

import * as crypto from 'crypto';

export function objectToMD5(obj: { [key: string]: any }): string {
    const jsonString = JSON.stringify(obj);
    const hash = crypto.createHash('md5');
    hash.update(jsonString);
    return hash.digest('hex');
}