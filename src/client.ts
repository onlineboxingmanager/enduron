import {BaseObject} from "@/models/BaseObject";
import axios from "axios";

import { navigate } from "svelte-routing";
import {notifications} from "@/notification";
import {objectToURLParams} from "@/helper";
import {auth} from "@/store";

export default class Client
{

	private static _auth: any
	private static _apiurl: string = 'https://enduron.ddev.site/api/';
	private static _token: string

	public static get auth(): any
	{
		Client.isAuth()

		return this._auth;
	}

	public static set auth(value: any)
	{
		this._auth = value;
	}

	public static get token(): string
	{
		Client.isAuth()

		return this._token;
	}

	public static set token(value: string)
	{
		this._token = value;
	}

	public static get apiurl(): string
	{
		return this._apiurl;
	}

	public static set apiurl(value: string)
	{
		this._apiurl = value;
	}

	public static doLogout(): void
	{
		Client.auth = null;
		localStorage.clear();
		auth.update((store: boolean) => store = false )
	}

	public static async login()
	{
		await Client.request("auth", "POST", [], {}, (response) =>
		{
			const is_auth = Client.doAuth(response)
			auth.update((store) => store = is_auth )
		});

		return false;
	}

	public static doAuth(response: any): boolean
	{
		if( typeof response['expires_in'] === 'undefined' || typeof response['access_token'] === 'undefined' )
			return false;

		let time = new Date();
		time.setSeconds(time.getSeconds() + response?.expires_in);
		response.expires_at = time.toUTCString();

		Client.auth = response;
		Client.token = response.access_token;
		console.log('auth', response)
		localStorage.setItem('_auth', JSON.stringify(response));

		return true;
	}

	public static _checkAuth()
	{
		const isAuth = Client.isAuth();

		if (isAuth)
		{
			return this.doLogout();
		}
	}

	public static isAuth(): boolean
	{
		const auth = localStorage.getItem('_auth');

		if (!auth)
		{
			return false;
		}

		Client._auth = JSON.parse(auth);
		Client._token = Client._auth.access_token;

		let expire_date = new Date(Client._auth.expires_at).getTime();
		let now = new Date().getTime();

		if( expire_date <= now )
			Client.doLogout();

		return expire_date >= now;
	}

	static async request(url: string, method: string = 'GET', params: any = {}, headers: any = {}, callback?: any)
	{
		//console.warn(":: ++++ CALL API " + url);

		headers['Authorization'] = Client.auth ? 'Bearer ' + Client.token : 'Basic ' + btoa(params.email + ':' + params.password);

		if( typeof headers['Content-Type'] == 'undefined' )
			headers['Content-Type'] = "application/json;charset=UTF-8"

		if ( method == 'GET' && params )
		{
			let queryString = objectToURLParams(params);
			//let queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');
			url += '?' + queryString;
		}

		const options = {
			method: method,
			url: Client.apiurl + url,
			data: params ?? [],
			headers: headers,
		};

		let response = await axios.request(options)
			.catch(async (error) => {
			console.error('axios', error);

			if( error.response.data.code === 401 )
			{
				console.log(":: Token is not valid - relogging now");
				this.doLogout()
				await this.login();
				return false
			}

			let errormsg = error.response.data;

			if ( error.response.data.code && error.response.data.code == 23000 )
			{
				// doppelter Datensatz / ist bereits vorhanden
			}

			if( error.response.data.message )
				errormsg = error.response.data.message

			notifications.danger(errormsg, 5000)

			return false;

		}).then((data: any) =>
		{
			if( callback )
				callback(data.data)

			return data.data
		});

		return response;
	}

}