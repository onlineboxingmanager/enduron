import { writable } from 'svelte/store';

export let storeFE = writable(new Map());
export let auth = writable(false);

export function updateStoreOutsideSvelte(className, uid, myModel, action = 'update')
{
	storeFE.update((storeData) => {
		// Erstelle eine flache Kopie des storeData
		const updatedData = new Map(storeData);

		// Überprüfe, ob die Klasse bereits im Store vorhanden ist
		if (!updatedData.has(className)) {
			updatedData.set(className, new Map());
		}

		// Aktualisiere die innere Map mit den Daten des Modells
		if( action == 'update' )
			updatedData.get(className).set(uid, myModel);

		if( action == 'delete' )
			updatedData.get(className).delete(uid, myModel);

		//console.log( action + " von extern " + className + " " + uid, myModel );

		return updatedData;
	});
}