# Enduron Framework

Enduron ist ein leichtgewichtiges PHP-Framework, das schnelle und flexible Entwicklung ermöglicht. Es bietet eine einfache Autoloading-Mechanik über Composer und ein CLI-Tool zur Verwaltung des Projekts.

Der Entwickler rät zur Verwendung der Legacy-Version ohne Namespaces. Zu viele Dateien im Autoloader sind schlecht für die Performance. Ein gezieltes Laden der Klassen nach Anwendungsfall kann über die Library-Klasse erfolgen.

## Inhaltsverzeichnis

- [Installation](#installation)
- [Verwendung](#verwendung)
    - [Autoloading](#autoloading)
    - [CLI-Tool](#cli-tool)
- [Demodaten](#demodaten)
- [Ordnerstruktur](#ordnerstruktur)
- [Mitwirken](#mitwirken)
- [Lizenz](#lizenz)

## Installation

Um das Enduron Framework zu deinem Projekt hinzuzufügen, verwende Composer:

```sh
composer require enduron/framework-base
```

Core und ORMCompiler sind enthalten. Solltest Du diese einzeln brauchen, kannst du folgende Commands nutzen:
```sh
composer require enduron/core
composer require enduron/ormcompiler
```

## Demodaten
Um das Framework und seine Stärken kennen zu lernen empfehlen wir die Installation des Demo-Projektes. Z.B. mittels ddev:

```sh
# DDEV starten
ddev start

# demo daten importieren
ddev import-db --file=ddev_db_demo.sql.gz

# in shell wechseln
ddev ssh

# Erzeugen der Modells und Dateien
php enduron ormcompiler:run
```

für spezielle Einstellungen und Zielpfade der generierten Dateien kannst du die ENV-Variablen nutzen:

```env
# .env
ORMCOMPILER_OUTPUT_MODELS=/var/www/html/application/models
ORMCOMPILER_OUTPUT_JS=/var/www/html/src
ORMCOMPILER_USE_NAMESPACES=true
```