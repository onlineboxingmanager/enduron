<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width,initial-scale=1">

		<title>Enduron Demo</title>

		<meta name="robots" content="noindex" />

		@stack('css')
	</head>
	<body>

		{{ $content ?? '' }}
		@stack('scripts')

	</body>
</html>