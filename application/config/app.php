<?php
#ä
$DOCUMENT_ROOT = realpath(__DIR__ . '/../../' );
define ('GLOBAL_DOCUMENT_ROOT', $DOCUMENT_ROOT);

error_reporting ( E_ALL );
ini_set('display_errors', 'on'); // php errors
ini_set('log_errors', 'on');
ini_set("error_log",  $DOCUMENT_ROOT . '/application/logs/'.date('Y-m-d', time()) . '.log');

define ('GLOBAL_INCLUDE_ABSTRACTIONLAYER', $DOCUMENT_ROOT . '/application/models/');
define ('GLOBAL_INCLUDE_APPLICATIONLAYER', $DOCUMENT_ROOT . '/application/models/');
define ('GLOBAL_INCLUDE_SYSTEMLAYER', $DOCUMENT_ROOT . '/application/system/');
define ('GLOBAL_INCLUDE_CONFIG', $DOCUMENT_ROOT . '/application/config/');
define ('GLOBAL_IMAGES', $DOCUMENT_ROOT . '/images/');

define ('GLOBAL_VIEWS', $DOCUMENT_ROOT . '/application/views/');
define ('GLOBAL_CONTROLLER', $DOCUMENT_ROOT . '/application/http/controller/');
define ('GLOBAL_CACHE', $DOCUMENT_ROOT . '/application/cache/');