<?php
#

return [
	'default' =>
	[
		'GLOBAL_DB_DRIVER', getenv('DB_DRIVER') ?? 'db',
		'GLOBAL_DB_HOST', getenv('DB_HOST') ?? 'db',
		'GLOBAL_DB_PORT', getenv('DB_PORT') ?? 'db',
		'GLOBAL_DB_DBNAME', getenv('DB_DATABASE') ?? 'db',
		'GLOBAL_DB_USER', getenv('DB_USERNAME') ?? 'db',
		'GLOBAL_DB_PASS', getenv('DB_PASSWORD') ?? 'db'
	]
];