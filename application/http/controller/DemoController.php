<?php
#ä
namespace Enduron\HTTP\Controller;

use Enduron\Core\Controller\BaseController;
use Enduron\Core\Query\Criteria;
use Enduron\Models\ApiToken\ApiToken;
use Enduron\Models\ApiToken\ApiTokenQuery;
use Enduron\Models\Project\Project;
use Enduron\Models\Project\ProjectQuery;

class DemoController extends BaseController
{

    public function __construct()
    {
        parent::__construct();

		// set default layout
        $this->view->setLayout('public');
    }

    public function index()
    {
	    // get projects by all() methdo
	    $projectList = Project::all()->get();
	    foreach ( $projectList as $project )
	    {
		    // update attributes here and use save() method
		    // ....
		    $project->save();
	    }

	    // use query and auto joins
	    $myQuery = new ProjectQuery();
	    $myList = $myQuery
		    ->active()
		    ->get();

	    return $this->response('demo/index', ['projects' => $myList]);
    }
}