<?php
#ä
namespace Enduron\HTTP\Controller;

use Enduron\Core\Controller\BaseController;

class StartController extends BaseController
{

    public function __construct()
    {
        parent::__construct();

        $this->view->setLayout('public');
    }

    public function index()
    {
		$this->redirect('demo');
    }
}