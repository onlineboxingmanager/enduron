<?php
#?
namespace Enduron\HTTP\Controller;

use Enduron\Core\Api\ApiBaseController;

class ApiController extends ApiBaseController
{

    public const VERSION = "v1.0.0";

    public function __construct()
    {
        parent::__construct();
    }

	public function Language(?string $parameter = null)
	{
		return $this->_generic(\Enduron\Models\Language\Language::class, $parameter);
	}

	public function Currency(?string $parameter = null)
	{
		return $this->_generic(\Enduron\Models\Currency\Currency::class, $parameter);
	}

	public function Project(?string $parameter = null)
	{
		return $this->_generic(\Enduron\Models\Project\Project::class, $parameter);
	}

}