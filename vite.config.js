import { svelte } from "@sveltejs/vite-plugin-svelte";

import { defineConfig } from 'vite'
import mkcert from 'vite-plugin-mkcert'
import sveltePreprocess from 'svelte-preprocess';
import {resolve} from "path";
import {glob} from "glob";

/** @type {import('vite').UserConfig} */
const config = {
	server: {
		host: '0.0.0.0',
		port: 3000,
		https: true
	},
	base: '',
	publicDir: 'public',
	build: {
		manifest: true,
		emptyOutDir: true,
		copyPublicDir: false,
		outDir: '../public',
		rollupOptions: {
			input: [
				'src/main.ts',
			],
			output: {
				entryFileNames: `[name].js`,
				chunkFileNames: `[name].js`,
				assetFileNames: `[name].[ext]`
			}
		}
	},
	resolve:{
		alias:{
			'@' : resolve(__dirname, './src')
		},
	},
	plugins: [
		{
			name: 'html',
			handleHotUpdate({file, server}) {
				if (file.endsWith('.svelte') || file.endsWith('.html')) {
					server.ws.send({
						type: 'full-reload',
						path: '*'
					});
				}
			}
		},
		mkcert(),
		svelte({
			preprocess: [sveltePreprocess()],
		}),
	],

	optimizeDeps: {exclude: ["svelte-navigator"]}
}

export default defineConfig(config)